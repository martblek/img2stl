#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Konvertuje obrázek na Stereolithography objekt
"""

import struct
from math import sqrt
from Tkinter import Tk

__autor__ = "Martin Bílek, martblek@gmail.com"
__version__ = "0.02"
__license__ = "GPL"

root = Tk()
vertexes = {}


class Vertex:
    __slots__ = ('x', 'y', 'z')

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


def TriangulateFace(v1, v2, v3, v4):
    return(v1, v2, v3), (v3, v4, v1)


def CalcNormal(v1, v2, v3):
    a = [v2[0]-v1[0], v2[1]-v1[1], v2[2]-v1[2]]
    b = [v3[0]-v1[0], v3[1]-v1[1], v3[2]-v1[2]]
    n = [a[1]*b[2] - a[2]*b[1], a[2]*b[0]-a[0]*b[2], a[0]*b[1]-a[1]*b[0]]
    nlen = sqrt(n[0]**2+n[1]**2+n[2]**2)
    return [n[0]/nlen, n[1]/nlen, n[2]/nlen]


def CreateBox(fle, im, stepx, stepy, height, normals=False):
    w, h = im.size
    x1 = 0.0
    x2 = (w-1) * stepx
    y1 = 0.0
    y2 = -((h-1) * stepy)
    z1 = 0.0
    z2 = height

    f = {}
    f[0] = ((x1, y1, z1), (x1, y1, z2), (x2, y1, z2), (x2, y1, z1))   # TOP
    f[1] = ((x1, y1, z1), (x1, y2, z1), (x1, y2, z2), (x1, y1, z2))  # LEFT
    f[2] = ((x2, y1, z1), (x2, y1, z2), (x2, y2, z2), (x2, y2, z1))   # RIGHT
    f[3] = ((x1, y2, z1), (x2, y2, z1), (x2, y2, z2), (x1, y2, z2))  # BOTTOM
    f[4] = ((x1, y1, z1), (x1, y2, z1), (x2, y2, z1), (x2, y1, z1))

    for x in range(5):
        tri = TriangulateFace(f[x][0], f[x][1], f[x][2], f[x][3])
        for n in range(len(tri)):
            if normals == 'true':
                n1, n2, n3 = CalcNormal(tri[n][0], tri[n][1], tri[n][2])
            else:
                n1 = n2 = n3 = 0

            data = [n1, n2, n3, tri[n][0][0], tri[n][0][1], tri[n][0][2],
                    tri[n][1][0], tri[n][1][1], tri[n][1][2],
                    tri[n][2][0], tri[n][2][1], tri[n][2][2], 0]

            fle.write(struct.pack("12fH", *data))


def LoadImage(image, filters):

    from PIL import Image, ImageFilter
    try:
        im = Image.open(image)
        w, h = im.size
        w1 = w + 2 if (w % 2) == 0 else w + 3
        h1 = h + 2 if (h % 2) == 0 else h + 3
        im1 = Image.new('L', (w1, h1), color=0)
        im1.paste(im, box=(1, 1))

        if filters:
            if filters == 'blur':
                im2 = im1.filter(ImageFilter.BLUR)
            elif filters == 'contour':
                im2 = im1.filter(ImageFilter.CONTOUR)
            elif filters == 'detail':
                im2 = im1.filter(ImageFilter.DETAIL)
            elif filters == 'edge_enhance':
                im2 = im1.filter(ImageFilter.EDGE_ENHANCE)
            elif filters == 'edge_enhance_more':
                im2 = im1.filter(ImageFilter.EDGE_ENHANCE_MORE)
            elif filters == 'emboss':
                im2 = im1.filter(ImageFilter.EMBOSS)
            elif filters == 'find_edges':
                im2 = im1.filter(ImageFilter.FIND_EDGES)
            elif filters == 'smooth':
                im2 = im1.filter(ImageFilter.SMOOTH)
            elif filters == 'smooth_more':
                im2 = im1.filter(ImageFilter.SMOOTH_MORE)
            elif filters == 'sharpen':
                im2 = im1.filter(ImageFilter.SHARPEN)
            else:
                return im1

            return im2
        return im1
    except:
        print('Error in loading picture.\n')
        return None


def CreateVertexes(im, z=0.0, box=0.0):
    info = {}
    info['res'] = (root.winfo_screenwidth(), root.winfo_screenheight())
    info['phys'] = (root.winfo_screenmmwidth(), root.winfo_screenmmheight())
    info['dpi_x'] = info['res'][0] / (info['phys'][0] / 25.4)
    info['dpi_y'] = info['res'][1] / (info['phys'][1] / 25.4)

    w, h = im.size
    mi, ma = im.getextrema()
    mmx = (w * 25.4) / info['dpi_x']
    mmy = (h * 25.4) / info['dpi_y']
    stepx = mmx / w
    stepy = mmy / h
    stepz = z / ma

    global DEBUG
    if DEBUG:
        print('\nImage resolution: %s x %s px.' % (w, h))
        print('Your screen resolution: %s x %s.' % (info['res'][0], info['res'][1]))
        print('Physical screen resolution: %s mm x %s mm.' % (info['phys'][0], info['phys'][1]))
        print('Your DPI in X: %s' % info['dpi_x'])
        print('Your DPI in Y: %s' % info['dpi_y'])
        print('Result dimensions will be : %s x %s mm.' % (mmx, mmy))
        print('X ratio                   : %s mm.' % stepx)
        print('Y ratio                   : %s mm.\n' % stepy)

    for height in range(h):
        for width in range(w):
            pnt = im.getpixel((width, height))
            vertexes[(width, height)] = \
                Vertex(width * stepx, -(height * stepy), (pnt * stepz) + box)

    return vertexes, w, h, stepx, stepy


def Export_STL(filename, z, box, normals, filters, export):
    image = LoadImage(filename, filters)
    if not image:
        return

    vs, w, h, stepx, stepy = CreateVertexes(image, z=z, box=box)
    if(box != 0.0):
        faces = (w-1)*(h-1)+5  # 10 faces navic, v cyklu niz nasobim 2mi /5*2/
    else:
        faces = (w-1) * (h-1)

    string = b"File Created with Img2STL by martblek@gmail.com"
    filename, end = filename.split('.')
    with open(export + '.stl', 'wb') as f:
        f.write(struct.pack("80sI", string, faces*2))
        for y in range(h-1):
            for x in range(w-1):
                tri = TriangulateFace(
                    (vs[(x, y)].x, vs[(x, y)].y, vs[(x, y)].z),
                    (vs[(x, y + 1)].x, vs[(x, y + 1)].y, vs[(x, y + 1)].z),
                    (vs[(x + 1, y + 1)].x, vs[(x + 1, y + 1)].y, vs[(x + 1, y + 1)].z),
                    (vs[(x + 1, y)].x, vs[(x + 1, y)].y, vs[(x + 1, y)].z))

                for n in range(len(tri)):
                    if normals == 'true':
                        n1, n2, n3 = CalcNormal(tri[n][0], tri[n][1], tri[n][2])
                    else:
                        n1 = n2 = n3 = 0

                    data = [n1, n2, n3,
                            tri[n][0][0], tri[n][0][1], tri[n][0][2],
                            tri[n][1][0], tri[n][1][1], tri[n][1][2],
                            tri[n][2][0], tri[n][2][1], tri[n][2][2], 0]
                    f.write(struct.pack("12fH", *data))
        if(box != 0.0):
            CreateBox(f, image, stepx, stepy, box, normals=normals)
    print("Stereolithography Binary STL file '%s' created.\n\n" % (filename+'.stl'))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Img23D %s\nby martblek@gmail.com' % __version__)
    parser.add_argument('--height', default=3.0, type=float, help='Maximální výška objektu bez podstavce.')
    parser.add_argument('--debug', default=True, choices=['True', 'False'], help='Informace o konverzi')
    parser.add_argument('--box', default=2.0, type=float, help='Výška boxu pod objektem.')
    parser.add_argument('--normals', default=False, choices=['True', 'False'], help='Počítat normály.')
    parser.add_argument('--input', required=True, help='Název obrázku.')
    parser.add_argument('--output', required=True, help='Název výstupního STL souboru.')
    parser.add_argument('--filter', default=False, choices=['blur', 'contour', 'detail', 'edge_enhance', 'edge_enhance_more', 'emboss', 'find_edges', 'smooth', 'smooth_more', 'sharpen'], help='Typ filtru.')

    args = parser.parse_args()
    x = vars(args)
    DEBUG = x['debug']
    Export_STL(x['input'], x['height'], x['box'], x['normals'], x['filter'], x['output'])